﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;

#if UNITY_IOS
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
#endif

[RequireComponent(typeof(AudioSource))]
public class GameManager : MonoBehaviour 
{
	public float GameDelay = 3; 			// Game start delay;

	public Transform bombFX;				//Bomb hit particle effect;
	public Transform candyFX;				//Candy hit particle effect;

	public Transform[] spawnPoints;			//Bombs/candies spawn points;
	public float spawnDelay = 0.5F;			//Spawn delay


	public Rigidbody2D[] Candies;			//Bombs/candies prefabs;
	public int preloadCandies = 20;			//Preload bombs/candies count for each in the array;
	public float yForceMin = 350F;			//Minimum force in Y axis;
	public float yForceMax = 350.0F;		//Maximum force in Y axis;
	public float xForceMin = 0;				//Minimum force in X axis;
	public float xForceMax = 0;				//Maximum force in X axis;
	public Transform BorderPrefab;			//Game area border prefab;
	public int Lifes = 3;					
	public UI uI;							//UI class;
	public SoundEffects soundEffects;		//SoundEffects class;
	public CameraShake cameraShake;			//CameraShake script;
	public Transform TouchTrail;			//Touch trail (GameObject with a trail renderer);

	[HideInInspector]
	public int Score;
	private float rate;
	private string popUpText;
	private Vector2 touchPos;
	private List<Rigidbody2D> preLoadedCandies = new List<Rigidbody2D>();
	private GUIText[] preLoadedPopUps;
	private Transform[] preLoadedBombFX;
	private Transform[] preLoadedCandyFX;
	private Vector3 followPos;

	private int curPopUp;
	private int curBombFX;
	private int curCandyFX;
	private int randomSpawnPoint; 
	private int randomCandie;
	
	[HideInInspector]
	public int defaultLifes;
	[HideInInspector]
	public float defaultGameDelay;

	private RaycastHit2D hit;
	private Camera cam;
//	private Transform[] Borders = new Transform[2];
	private Transform[] Borders = new Transform[2];
	

	private bool GameOver;
	[HideInInspector]
	public bool isPlaying;
	[HideInInspector]
	public bool isPaused;
	Color32 color;

	private bool isGameOver = false;

	void Awake()
	{
		//Preload all needed prefabs;
		InstantiatePrefabs();
		GameData.gameManager = this;
		color = new Color32();
		color.r = 90;
		color.g = 64;
		color.b = 143;
		color.a = 255;
	}

	// Use this for initialization
	void Start () 
	{
		//Assign default game values;
		defaultLifes = Lifes;
		defaultGameDelay = GameDelay;

		cam = Camera.main;	//Get main camera;
	
		//Instatiate TouchTrail prefab for using it in the future;
		TouchTrail = (Transform)Instantiate(TouchTrail, Vector3.zero, Quaternion.identity); 

		//Positioning border prefabs with left and right camera viewport bounds in X axis and zero in Y;  
		Borders[0].position = new Vector2 (cam.ViewportToWorldPoint(new Vector2(1,0)).x, 0);
		Borders[1].position = new Vector2 (cam.ViewportToWorldPoint(new Vector2(0,0)).x, 0);
		uI.LifeUI.text = Texts.GetText(WhatText.Life) + Lifes;
		uI.ScoreUI.text = Texts.GetText(WhatText.Score) + Score;
	}
	
	// Update is called once per frame
	void Update()
	{
		//UI logic function;
		UILogic();

		//Gameplay code;
		if (!isPaused && isPlaying && !GameOver && Input.GetMouseButton(0))
		{
			//Get mouse position and draw touch trail;
			followPos = cam.ScreenToWorldPoint(Input.mousePosition);
			followPos.z = 0;
			TouchTrail.position = followPos;
			
			hit = Physics2D.Raycast(cam.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

			//Destroy candies when hit;
			if(hit.collider != null)
			{
				if(hit.collider.CompareTag("Candy"))
				{
					DrawScore(hit.point, 10, color);
					UpdateScore(10);
					PlayEffects(soundEffects.explosionSource.GetComponent<AudioSource>(), soundEffects.CollectSFX);
					DrawCandyFX(hit.point);
					hit.collider.gameObject.SetActive(false);
				}
				
				if(hit.collider.CompareTag("Bomb"))
				{
					cameraShake.Shake();
					Lifes --;
					uI.LifeUI.text = Texts.GetText(WhatText.Life) + Lifes;
					PlayEffects(soundEffects.explosionSource.GetComponent<AudioSource>(), soundEffects.ExplosionSFX);
					DrawBombFX(hit.point);
					hit.collider.gameObject.SetActive(false);
				}
			}
		}
	}

	void PlayEffects(AudioSource source, AudioClip clip) {
		if(GameData.soundOn) {
			source.clip = clip;
			source.Play();
		}
	}

	void FixedUpdate () 
	{
		//Draw wait seconds;
		if(GameDelay >= 1) {
			isPlaying = false;
			uI.WaitText.enabled = true;
			GameDelay -= 1*Time.deltaTime;
		} else {
			uI.WaitText.enabled = false;
			isPlaying = true;
		}

		//Throw random candies;
		if(isPlaying && !GameOver && Time.time > rate)
		{
			rate = Time.time + spawnDelay;
			randomSpawnPoint = Random.Range(0, spawnPoints.Length);
			randomCandie = Random.Range(0, preLoadedCandies.Count);

			if (!preLoadedCandies[randomCandie].gameObject.activeSelf)
			{
				preLoadedCandies[randomCandie].transform.position = spawnPoints[randomSpawnPoint].position;
				preLoadedCandies[randomCandie].gameObject.SetActive(true);
				preLoadedCandies[randomCandie].AddForce(new Vector2(Random.Range(xForceMin, xForceMax), Random.Range(yForceMin, yForceMax)));
				preLoadedCandies[randomCandie].angularVelocity = -500;
				PlayEffects(GetComponent<AudioSource>(), soundEffects.throwSFX);
			}
			else
				randomCandie++;
		}
	}


	public void UILogic()
	{
		//Assign variables;
		uI.WaitText.text = GameDelay.ToString("F0");
		//Pause logic;
		if(isPaused)
		{
			Time.timeScale = 0.0001F;
		}
		else
		{
			Time.timeScale = 1;
		}


		//Game over logic;
		if(Lifes < 1)
			GameOver = true;
		else
			GameOver = false;

		if(GameOver) 
            //&&!GameData.showVideo) 
        {
			GameOverM();
		}
	}

	private void GameOverM() {
		if(!isGameOver) {
			isGameOver = true;
			MyAppodeal eee = FindObjectOfType<MyAppodeal> ();
			if (eee != null)
				eee.ShowADS ("ShowInterstitital");
			else
				print ("No ADS data!!!");
			
			for(int i = 0; i < preLoadedCandies.Count; i++)
			{
				preLoadedCandies[i].gameObject.SetActive(false);
			}
			if (!PlayerPrefs.HasKey("Score") && Score > 0)
			{
				PlayerPrefs.SetInt("Score", Score);
				PlayerPrefs.Save ();
				GameData.menuManager.currentScore.text = Texts.GetText(WhatText.YouScore)+"\n"+Score;
				GameData.menuManager.newRecord.SetActive(true);
			} else if (PlayerPrefs.GetInt("Score") < Score) {
				PlayerPrefs.SetInt("Score", Score);
				PlayerPrefs.Save ();
				GameData.menuManager.currentScore.text = Texts.GetText(WhatText.YouScore)+"\n"+Score;
				GameData.menuManager.newRecord.SetActive(true);
			} else {
				GameData.menuManager.currentScore.text = Texts.GetText(WhatText.YouScore)+"\n"+Score;
				GameData.menuManager.youScore.SetActive(true);
			}
		}
	}

	public void UpdateScore(int score)
	{
		Score += score;
		if (Score < 0)
			Score = 0;
		uI.ScoreUI.text = Texts.GetText(WhatText.Score) + Score;
		GameData.currentScore = Score;
	}

	//Instantiate all the prefabs;
	public void InstantiatePrefabs()
	{
		Vector2 vectorForText = new Vector2 (-10, -10);
		preLoadedPopUps = new GUIText[15];
		for(int i = 0; i < preLoadedPopUps.Length; i++)
		{
			preLoadedPopUps[i] = (GUIText)Instantiate(uI.ScorePopup, vectorForText, Quaternion.identity);
			preLoadedPopUps[i].transform.parent = transform;
//			preLoadedPopUps[i].gameObject.SetActive(false);
		}
		
		preLoadedBombFX = new Transform[5];
		for(int i = 0; i < preLoadedBombFX.Length; i++)
		{
			preLoadedBombFX[i] = (Transform)Instantiate(bombFX, Vector3.zero, Quaternion.identity);
			preLoadedBombFX[i].transform.parent = transform;
			preLoadedBombFX[i].gameObject.SetActive(false);
		}
		
		preLoadedCandyFX = new Transform[10];
		for(int i = 0; i < preLoadedCandyFX.Length; i++)
		{
			preLoadedCandyFX[i] = (Transform)Instantiate(candyFX, Vector3.zero, Quaternion.identity);
			preLoadedCandyFX[i].transform.parent = transform;
			preLoadedCandyFX[i].gameObject.SetActive(false);
		}
		
		foreach (Rigidbody2D candie in Candies)
		{
			for (int i=0; i < preloadCandies; i++)
			{
				Rigidbody2D r = (Rigidbody2D)Instantiate(candie, Vector3.zero, Quaternion.identity);
				r.transform.parent = transform;
				preLoadedCandies.Add(r);
				r.gameObject.SetActive(false);
			}
		}
		
		for(int i = 0; i < Borders.Length; i++)
		{
			Borders[i] = (Transform)Instantiate(BorderPrefab, Vector2.zero, Quaternion.identity);
		}
	}

	//Draw particle effects functions;
	public void DrawBombFX(Vector3 position)
	{
		if(curBombFX >= preLoadedBombFX.Length)
			curBombFX = 0;

		if (!preLoadedBombFX[curBombFX].gameObject.activeSelf)
		{
			preLoadedBombFX[curBombFX].gameObject.SetActive(true);
			preLoadedBombFX[curBombFX].position = position;
			curBombFX++;
		}
	}

	public void DrawCandyFX(Vector3 position)
	{
		if(curCandyFX >= preLoadedCandyFX.Length)
			curCandyFX = 0;

		if (!preLoadedCandyFX[curCandyFX].gameObject.activeSelf)
		{
			preLoadedCandyFX[curCandyFX].gameObject.SetActive(true);
			preLoadedCandyFX[curCandyFX].position = position;
			curCandyFX++;
		}
	}

	//Draw score popup function;
	public void DrawScore(Vector2 pos, int scoreValue, Color col)
	{
		if(curPopUp >= preLoadedPopUps.Length)
			curPopUp = 0;
		if (!preLoadedPopUps[curPopUp].gameObject.activeSelf)
		{
			preLoadedPopUps[curPopUp].gameObject.SetActive(true);
			preLoadedPopUps[curPopUp].transform.position = cam.WorldToViewportPoint(pos);
			preLoadedPopUps[curPopUp].color = col;
			if(scoreValue > 0) 
				popUpText = "+" + scoreValue.ToString();
			else 
				popUpText = scoreValue.ToString();

			preLoadedPopUps[curPopUp].text = popUpText;
			curPopUp++;
		}
	}

	//Play sound function;
	void PlaySound(AudioClip ac)
	{
		if(ac && GameData.soundOn)
		{
			GetComponent<AudioSource>().clip = ac;
			GetComponent<AudioSource>().Play();
		}
	}

    public  int GetScore()
    {
        return Score;
    }
}

//Some classes to organize inspector;

[System.Serializable]
public class SoundEffects
{
	public AudioClip throwSFX;				//Bombs/candies instantiation sound effect;
	public AudioClip ExplosionSFX;			//Bomb explosion sound;
	public AudioSource explosionSource;		//Bomb explosion Audio Source;
	public AudioClip CollectSFX;			//Candy collect sound;
	public AudioSource collectSource;		//Candy collect Audio Source;
	public AudioClip clickSound;			//Buttons click sound;

}

[System.Serializable]
public class UI
{
	public GUIText  ScorePopup, WaitText;
	public Text ScoreUI, LifeUI;

}


