﻿using UnityEngine;
using System.Collections;

public class ScorePopup : MonoBehaviour 
{

	public float Speed = 1;
	private Color defaultColor;

	void Start () 
	{
		defaultColor = GetComponent<GUIText>().color;
	}
	
	void FixedUpdate () 
	{
		//Fade color alpha;
		Color col = GetComponent<GUIText>().color;
		col.a -= 1*Time.deltaTime;
		GetComponent<GUIText>().color = col;

		//Move object up while alpha isn't 0 and if so, reset its color and disable so it can be used again;
		if(col.a > 0)
		{
			transform.Translate(Vector3.up * Speed/1000);
		}
		else
		{
			gameObject.SetActive(false);
			GetComponent<GUIText>().color = defaultColor;
		}
	}
}
