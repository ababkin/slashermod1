﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour {



	void Start () {
		if (PlayerPrefs.HasKey ("sound")) {
			GameData.soundOn = System.Convert.ToBoolean (PlayerPrefs.GetInt ("sound"));
		}
		if (PlayerPrefs.HasKey ("music")) {
			GameData.musicOn = System.Convert.ToBoolean (PlayerPrefs.GetInt ("music"));
		}
        StartCoroutine(StartScene(1));

    }

    void Update() {
        
    }
	
	IEnumerator StartScene(float waitTime) {
		yield return new WaitForSeconds (waitTime);
//#if UNITY_EDITOR
        SceneManager.LoadScene("MainMenu");
/*#elif UNITY_ANDROID && !UNITY_ANDROID
        FindObjectOfType<MyAppodeal>().ShowADS("ShowInterstitital");
#endif*/
    }
}
