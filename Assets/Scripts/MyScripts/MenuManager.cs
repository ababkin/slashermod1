﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuManager : MonoBehaviour {
	public AudioClip clickSound;
	public GameObject settings;
	public GameObject pause;
	public GameObject youScore;
	public GameObject newRecord;
	public Text currentScore;
	// Use this for initialization
	void Awake () {
		GameData.menuManager = this;
	}

	public void PlaySound(AudioClip ac)
	{
		if(GameData.soundOn) {
			if(ac) {
				GetComponent<AudioSource>().clip = ac;
				GetComponent<AudioSource>().Play();
			}
		}

	}
}
