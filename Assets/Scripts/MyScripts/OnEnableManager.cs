﻿using UnityEngine;
using System.Collections;

public enum ObjectName {
	Settings,
	Quit,
	Pause,
	EndLife,
	NewRecord
}

public class OnEnableManager : MonoBehaviour {
	public ObjectName popupName;
	void OnEnable () {
		GameData.escapeBtnManager.openedWindows.Add (this.gameObject);
		if(popupName == ObjectName.Pause) {
			GameData.gameManager.isPaused = true;
		} else if(popupName == ObjectName.Settings && GameData.gameManager != null) {
			GameData.gameManager.isPaused = true;
		}
	}

	void OnDisable () {
		GameData.escapeBtnManager.openedWindows.Remove (this.gameObject);
		if (popupName == ObjectName.Settings && GameData.gameManager != null) {
			GameData.gameManager.isPaused = false;
		}
	}
}
