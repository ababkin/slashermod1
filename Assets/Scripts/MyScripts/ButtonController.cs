﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
#if UNITY_IOS
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
#endif


public enum Settings{
	Sound,
	Music
}

public class ButtonController : MonoBehaviour {
	public void PlayGame() {
		//GameData.showVideo = false;
		Application.LoadLevel("DemoLevel");
	}

	public void Settings() {
		GameData.menuManager.settings.SetActive (true);
	}

	public void HiddenPopup() {
		this.GetComponentInParent<Canvas>().gameObject.SetActive (false);
	}

	public void ClickSound() {
		GameData.menuManager.PlaySound(GameData.menuManager.clickSound);
	}

	public void QuitGame() {
		Application.Quit ();
	}

	public void Pause() {
		GameData.menuManager.pause.SetActive (true);
	}

	public void MainMenu() {
		/*if (!GameData.showVideo) {
			#if UNITY_IOS
			Appodeal.show (Appodeal.INTERSTITIAL);
			#endif
		} */
		Application.LoadLevel("MainMenu");
	}

	public void Resume() {
		GameData.gameManager.isPaused = false;
		HiddenPopup ();
	}

	public void Replay() {
		GameData.gameManager.isPlaying = false;
		GameData.gameManager.Lifes = GameData.gameManager.defaultLifes;
		GameData.gameManager.GameDelay = GameData.gameManager.defaultGameDelay;
		GameData.gameManager.Score = 0;
		GameData.currentScore = 0;
		//GameData.showVideo = false;
		Application.LoadLevel("DemoLevel");
	}

	public void Sound() {
		Toggle toggle = GetComponent<Toggle> ();
		Settings (global::Settings.Sound, toggle);
	}

	public void Music() {
		Toggle toggle = GetComponent<Toggle> ();
		Settings (global::Settings.Music, toggle);
	}

	void Settings(Settings settings, Toggle toggle) {
		switch (settings) {
		case global::Settings.Music:
			PlayerPrefs.SetInt("music",System.Convert.ToInt32(toggle.isOn));
			GameData.musicOn = toggle.isOn;
			if(toggle.isOn) {
				SoundInit.source.Play ();
			} else {
				SoundInit.source.Pause();
			}
			break;
		case global::Settings.Sound: 
			PlayerPrefs.SetInt("sound",System.Convert.ToInt32(toggle.isOn));
			GameData.soundOn = toggle.isOn;
			break;
		}
		PlayerPrefs.Save ();
	}


}
