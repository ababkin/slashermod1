﻿using UnityEngine;

public enum WhatText
{
	Play,
	Record,
	BestScore,
	NoRecords,
	Ok,
	Settings,
	Music,
	Sound,
	Quit,
	QuitText,
	Yes,
	No,
	Life,
	Score,
	YouScore,
	Resume,
	Pause,
	Exit,
	HappyHalloween,
	MainMenu,
	MainMenuText,
	Restart,
	GameOver,
	Share,
	NewRecord,
    PointsText1,
    PointsText2,
    PointsText3,
    ShareText
}

public enum Language {
	English,
	Russian
}

public static class Texts
{
	private static string[,] value = {
		{"Play","Играть"},
		{"Highscore","Рекорд"},
		{"Best score","Лучший счет"},
		{"No highscores","Нет рекордов"},
		{"Ok","Ок"},
		{"SETTINGS","НАСТРОЙКИ"},
		{"Music","Музыка"},
		{"Sounds","Звуки"},
		{"Quit","Выход"},
		{"Do you really want to quit?","Вы действительно хотите выйти?"},
		{"Yes","Да"},
		{"No","Нет"},
		{"Lives: ","Жизни: "},
		{"Score: ","Счет: "},
		{"Your score: ","Ваш счет: "},
		{"Resume","Назад"},
		{"Pause","Пауза"},
		{"EXIT","ВЫЙТИ"},
		{"Yo-ho-ho,\nand a bottle\nof rum!","Йо-хо-хо,\nи бутылка\nрому!"},
		{"Main menu","Выход"},
		{"Do you really want to get back to main menu?","Вы действительно хотите выйти в главное меню?"},
		{"Restart","Заново"},
		{"Game over","Игра окончена"},
		{"Wow! I've reached {0} \tscored  in this game !","Ого! Я заработал {0} очков в этой игре!"},
		{"New record!","Новый рекорд!"},
        {"points","очко"},
        {"points","очков"},
        {"points","очка"},
        {"I've gained","Я набрал"},

	};

	public static string GetText (WhatText what)
	{
		return value [(int)what, (int)LangItem.language];
	}
}

public class LangItem : MonoBehaviour {
	public WhatText whatIsThis;
	public string textBefore, textAfter;
	public UnityEngine.UI.Text textUI;
	public static Language language {
		get {
			switch (Application.systemLanguage) {
			case SystemLanguage.Russian:
				return Language.Russian;
			default:
				return Language.English;
			}
		}
	}
	private static System.Collections.Generic.List<LangItem> all;
	public static void UpdateAll () {
		if (all != null) {
			foreach (LangItem item in all) {
				item.Change ();
			}
		}
	}
	private static void AddToAll (LangItem item) {
		if (all == null) {
			all = new System.Collections.Generic.List<LangItem> ();
		}
		all.Add (item);
	}
	private static void RemoveFromAll (LangItem item) {
		if (all != null) {
			all.Remove (item);
		}
	}

	void Awake() {
		GameData.langItem = this;
	}

	public void SetText (WhatText what) {
		whatIsThis = what;
		Change ();
	}

	void Change () {
		if (textUI == null) {
			textUI = this.GetComponent<UnityEngine.UI.Text> ();
		}
		textUI.text = textBefore+Texts.GetText(whatIsThis)+textAfter;
	}

	void OnEnable ()
	{
		AddToAll (this);
		Change ();
	}
	void OnDisable () {
		RemoveFromAll (this);
	}
}