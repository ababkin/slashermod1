﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EscapeButtonManager : MonoBehaviour {
	[HideInInspector]
	public List<GameObject> openedWindows = new List<GameObject>();
	public GameObject quit;

	void Awake() {
		GameData.escapeBtnManager = this;
	}

	void Update() {
		if (Input.GetKeyDown (KeyCode.Escape)) { 
			if(openedWindows.Count > 0){
				ObjectName oName = openedWindows[openedWindows.Count-1].GetComponent<OnEnableManager>().popupName;
				openedWindows[openedWindows.Count-1].SetActive(false);
				switch(oName) {
				case ObjectName.Settings:
					if (GameData.gameManager != null) {
						GameData.gameManager.isPaused = false;
					}
					break;
				case ObjectName.Quit: 
					break;
				case ObjectName.Pause:
					GameData.gameManager.isPaused = false;
					break;
				case ObjectName.NewRecord:
					Application.LoadLevel("MainMenu");
					break;
				case ObjectName.EndLife:
					Application.LoadLevel("MainMenu");
					break;
				}
			} else {
				quit.SetActive(true);
			}
		}
	}

}
