﻿using UnityEngine;
using System.Collections;

public static class GameData{

	public static MenuManager menuManager;
	public static EscapeButtonManager escapeBtnManager;

	public static bool soundOn = true;
	public static bool musicOn = true;

	public static GameManager gameManager;

	public static LangItem langItem;

	//public static bool showVideo = false;

	public static int currentScore;
}
