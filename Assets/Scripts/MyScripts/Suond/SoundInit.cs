﻿using UnityEngine;
using System.Collections;

public class SoundInit : MonoBehaviour {
	public static AudioSource source;
	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource> ();
		DontDestroyOnLoad (this.gameObject);
	}
}
