﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ToggleController : MonoBehaviour {
	public Settings settings;
	bool isOn;
	
	// Use this for initialization
	void Start () {
		switch(settings) {
		case Settings.Music: 
			isOn = GameData.musicOn;
			break;
		case Settings.Sound: 
			isOn = GameData.soundOn;
			break;
		}
		GetComponent<Toggle> ().isOn = isOn;
	}
}
