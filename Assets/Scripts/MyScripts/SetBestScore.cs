﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SetBestScore : MonoBehaviour {
	public Text text;
	public bool newRecord;
	public bool currentScore;
	LangItem lang;

	void OnEnable() {
		if (currentScore) {
			text.text = Texts.GetText (WhatText.Score) + "\n" + GameData.currentScore.ToString ("F0");
		} else {
			
			//Draw best score if it isn't null and higher then zero;
			if (PlayerPrefs.HasKey ("Score") && PlayerPrefs.GetInt ("Score") > 0) {
				if (newRecord) {
					text.text = Texts.GetText (WhatText.NewRecord) + ":\n" + PlayerPrefs.GetInt ("Score").ToString ("F0");
				} else {
					text.text = Texts.GetText (WhatText.BestScore) + ":\n" + PlayerPrefs.GetInt ("Score").ToString ("F0");
				}
			} else
				text.text = Texts.GetText (WhatText.NoRecords);
		}
	}

	void Start () {

	}

}
