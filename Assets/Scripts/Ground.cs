﻿using UnityEngine;
using System.Collections;

//This script used for the ground collider. The defauld tag and layer of ground collider should be - Ground;

public class Ground : MonoBehaviour 
{
	private GameManager gameManager;

	void Awake ()
	{
		gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
	}


	//If candy object collides with ground we can do some things with it, decrease score for example;

	void OnCollisionEnter2D( Collision2D col)
	{
		if(col.gameObject.CompareTag("Candy"))
		{
			gameManager.UpdateScore(-25);									//Decrease score
			gameManager.DrawScore(col.transform.position, -25, Color.red);	//Draw score popup;
			col.gameObject.SetActive(false);								//Disable collision objects (candies) so they can be used again;
		}
	}
}
