﻿using UnityEngine;
using System.Collections;


//A special zone to disable bombs falling bombs;

public class DeadZone : MonoBehaviour 
{
	void OnTriggerEnter2D (Collider2D col) 
	{
		col.gameObject.SetActive(false);
	}
}
