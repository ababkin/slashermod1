﻿using UnityEngine;
using System.Collections;


[ExecuteInEditMode]
public class TextureScale : MonoBehaviour {
	
	
	public Vector2 textureSize = new Vector2(0.65F, 0.15F);

	private GUITexture thisTexture;
	private float screenRatio;
	
	void Start () 
	{
		thisTexture = GetComponent<GUITexture>();
		ResizeGUI();
	}
	
	// Update is called once per frame
	void Update () 
	{
	#if UNITY_EDITOR
			ResizeGUI();
	#endif
	}
	
	void ResizeGUI()
	{
		screenRatio = (float)Screen.width/Screen.height;
		Rect size = thisTexture.pixelInset;
		size.width = textureSize.x*Screen.width;
		size.height = textureSize.y*Screen.height * screenRatio;
		size.x = -size.width/2;
		size.y = -size.height/2;
		thisTexture.pixelInset = size;
	}
}
