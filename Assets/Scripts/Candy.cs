﻿using UnityEngine;
using System.Collections;

public class Candy : MonoBehaviour {

	//Change sorting layer of the candy object, to make the effect like its fly from the pipes and drop in front of them.

	private SpriteRenderer sprite;
	
	void Start () {
		sprite = GetComponent<SpriteRenderer>();
		StartCoroutine("ChangeLayer");
	}
	
	IEnumerator ChangeLayer()
	{
		yield return new WaitForSeconds(1);
		sprite.sortingOrder = 2;
	}

	void OnCollisionEnter2D (Collision2D col)
	{
		if(col.gameObject.CompareTag("Ground"))
			sprite.sortingOrder = 0;
	}
}
