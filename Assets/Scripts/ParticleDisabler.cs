﻿using UnityEngine;
using System.Collections;

public class ParticleDisabler : MonoBehaviour {

	private ParticleSystem particle;

	void Start () 
	{
		particle = GetComponent<ParticleSystem>();
		particle.Play();
	}

	void Update () 
	{
		if(!particle.isPlaying)
			gameObject.SetActive(false);
	}
}
