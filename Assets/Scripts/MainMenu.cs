﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class MainMenu : MonoBehaviour 
{
	
	public GUITexture Play;
	public GUITexture Exit;
	public GUIText BestScore;
	public AudioClip clickSound;
	public int LoadIndex;
	private bool touch;
	private Vector3 touchPos;
	
	void Start () {
		//Draw best score if it isn't null and higher then zero;
		if(PlayerPrefs.HasKey("Score") && PlayerPrefs.GetInt("Score") > 0)
		{
			BestScore.enabled = true;
			BestScore.text = PlayerPrefs.GetInt("Score").ToString("F0");
		}
		else BestScore.text = "- -";
	}
	
	void Update () 
	{
		
		//Platforrm depending inputs;
		#if UNITY_IPHONE || UNITY_ANDROID || UNITY_WP8
		if(Input.GetKey(KeyCode.Escape))
		{
			Application.Quit();
		}

		if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
		{
			if(Play.GetComponent<GUITexture>().HitTest(Input.GetTouch(0).position))
			{
				PlaySound(clickSound);
				Application.LoadLevel(LoadIndex);
			}
			else if(Exit.GetComponent<GUITexture>().HitTest(Input.GetTouch(0).position))
			{
				PlaySound(clickSound);
				Application.Quit();
			}
		}
		#endif
		
		#if UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_WEBPLAYER || UNITY_EDITOR
		if(Play.GetComponent<GUITexture>().HitTest(Input.mousePosition) && Input.GetMouseButtonDown(0))
		{
			PlaySound(clickSound);
			Application.LoadLevel(LoadIndex);
		}
		else if(Exit.GetComponent<GUITexture>().HitTest(Input.mousePosition) && Input.GetMouseButtonDown(0))
		{
			PlaySound(clickSound);
			Application.Quit();
		}
		#endif
	}
	
	void PlaySound(AudioClip ac)
	{
		if(ac)
		{
			GetComponent<AudioSource>().clip = ac;
			GetComponent<AudioSource>().Play();
		}
	}
}