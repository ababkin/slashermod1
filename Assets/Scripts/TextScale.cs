﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class TextScale : MonoBehaviour {

	//The script changes GUItext scale according to the screen resolution. Note: Pixel Correct should be disabled.

	public int TextSize = 25;
	private Vector2 desiredSize = new Vector2(0.25F, 0.25F);
	private GUIText text;
	private float scaleFactor;
	
	void Start () {
		text = GetComponent<GUIText>();
		SetSize();
	}

	void Update () 
	{

	#if UNITY_EDITOR
		SetSize();
	#endif

	}
	
	void SetSize()
	{
		scaleFactor = (float)Screen.width/Screen.height;
		text.fontSize = TextSize;
		transform.localScale = new Vector3(desiredSize.x, scaleFactor * desiredSize.y, 1);
	}
}
