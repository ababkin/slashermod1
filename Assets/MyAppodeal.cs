﻿using System;
using UnityEngine;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using UnityEngine.SceneManagement;


public class MyAppodeal : MonoBehaviour, IInterstitialAdListener
{
    public string _key;

    void Start()
    {

        Init();
    }

    void Init()
    {
        Appodeal.setInterstitialCallbacks(this);
        Appodeal.disableLocationPermissionCheck();
      //  Appodeal.setTesting(true);
        Appodeal.initialize(_key, Appodeal.BANNER_BOTTOM | Appodeal.INTERSTITIAL);
        ShowADS("ShowBanner");
        if (SceneManager.GetActiveScene().name == "MainMenu")
         Appodeal.show(Appodeal.INTERSTITIAL);
       ShowADS("ShowInterstitital");
    }

    public void ShowADS(string _type)
    {
        switch (_type)
        {
            case "ShowBanner":
                Appodeal.show(Appodeal.BANNER_BOTTOM);
                break;
            case "HideBanner":
                Appodeal.hide(Appodeal.BANNER_BOTTOM);
                break;
            case "ShowInterstitital":
                if (Appodeal.isLoaded(Appodeal.INTERSTITIAL)) {
                    Appodeal.show(Appodeal.INTERSTITIAL);
                }
                break;
        }
    }

    //----------------------------------------

    public void onInterstitialClicked()
    {
        throw new NotImplementedException();
    }

    public void onInterstitialClosed()
    {

    }

    public void onInterstitialFailedToLoad()
    {

    }

    public void onInterstitialLoaded()
    {
        throw new NotImplementedException();
    }

    public void onInterstitialShown()
    {
        throw new NotImplementedException();
    }


}
